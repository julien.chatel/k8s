# TP Kubernetes ETRS701 - Site Wordpress avec base de données
## CHATEL Julien - BOIS Cédric | <i>M1 TRI_1 2020-2021</i>
---

<ins>**Objectif :**</ins> Déploiement d'un serveur web et de wordpress grace a Kubernetes  

### Récupération des fichiers

Tous les fichiers nécessaires au déploiement sont disponibles sur le [dépot gitlab](https://gitlab.com/julien.chatel/k8s/).

**1.** Il est également possible de cloner le projet avec la commande git :

```bash
git clone https://gitlab.com/julien.chatel/k8s.git
```

### Déploiement de l'application

**2.** Pour lancer l'application sur le cluster :

```bash
cd k8s/wordpress-TP
```
```bash
kubectl apply -k ./
```
ou 

```bash
kubectl apply -f namespace.yaml
```
```bash
kubectl apply -f mysql/pvc.yaml
```
```bash
kubectl apply -f mysql/secret.yaml
```
```bash
kubectl apply -f mysql/service.yaml
```
```bash
kubectl apply -f mysql/deployment.yaml
```
```bash
kubectl apply -f /wordpress/pvc.yaml
```
```bash
kubectl apply -f /wordpress/service.yaml
```
```bash
kubectl apply -f /wordpress/deployment.yaml
```



**3.** A la suite du 1er démarrage, il faut également initialiser la base de données :

```bash
cat bdd.sql | kubectl exec -i <mysql-pod-name> -- mysql -u root -ppassword
```

><ins>**Note:**</ins> \<sql-pod-name> est à remplacer par le nom du pod mysql qui vient d'être déployé. 
Celui-ci est récupérable par la commande :
>
>
>      kubectl get pods | grep mysql


### Test du volume persistant

Les 2 pods sont chacun liés à un volume persistant, ce qui signifie qu'en cas de crash ou de suppression d'un pod les données ne seront pas perdues.

Il est possible de réaliser le test en supprimant un des 2 pods via le dashboard ou grâce à la commande suivante :

```bash
kubectl delete pods name=<pod-name>
```

Si tout s'est bien passé, un nouveau pod a été déployé automatiquement et le volume persistant associé est remonté, assurant ainsi la disponibilité des données.


